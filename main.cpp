#include <SFML/Graphics.hpp>
#include "Sassy/Sassy.h"

int main(int argc, char** argv)
{
	sf::RenderWindow window(sf::VideoMode(800, 600), "Sassy", sf::Style::Close);

	//set this to external if you want the windows to be drawn in order
	//however you must then use the static methods processWindows and drawWindows
	sy::Window::mode = sy::Window::External;

	//the window object inherits from sy::GUI so it can contain it's own children
	//Create a window object, can be dragged and contains it's own child elements
	sy::Window demo(sf::Vector2f(400, 300), sf::Vector2f(300, 300), "demo");
	demo.setFocused();

	//add some elements to the window
	demo.addElement("label", sy::Label(sf::Vector2f(150, 50), "set this with the text box"));

	//grab the button for use later.
	sy::Button& button = demo.addElement("button", sy::Button(sf::Vector2f(225, 100), sf::Vector2f(64, 32), "set"));

	demo.addElement("textbox", sy::TextBox(sf::Vector2f(75, 100), sf::Vector2f(128, 32), 20));

	demo.addElement("label2", sy::Label(sf::Vector2f(150, 180), "cool check box and slider"));
	sy::CheckBox& checkbox = demo.addElement("checkbox", sy::CheckBox(sf::Vector2f(150, 225)));
	demo.addElement("slider", sy::Slider(sf::Vector2f(150, 275), sf::Vector2f(128, 24)));

	//make the second window
	sy::Window themeWindow(sf::Vector2f(650, 200), sf::Vector2f(200, 300), "pick a theme!");

	sy::RadioGroup& group = themeWindow.addElement("group", sy::RadioGroup(sf::Vector2f(50, 100), 4));
	themeWindow.addElement("label", sy::Label(sf::Vector2f(125, 150), "Steel\nWater Melon\nDeep Sea\nRoyal"));

	while(window.isOpen())
	{
		//update the visiblity of the theme window
		themeWindow.visible = checkbox.checked;

		sf::Event event;
		while(window.pollEvent(event))
		{
			//distribute events to gui elements
			sy::Window::processWindows(event);

			if(event.type == sf::Event::Closed)
			{
				window.close();

			}

		}

		//check if the button was pressed
		if(button.isPressed())
		{
			//if the button was pressed get the label and textbox
			sy::Label& label = (sy::Label&)demo.getElement("label");
			sy::TextBox& textbox = (sy::TextBox&)demo.getElement("textbox");
			//set the label to the contents of the textbox
			label.setString(textbox.str);

		}

		if(themeWindow.visible)
		{
			if(group.hasSwitched())
			{
				unsigned int themeIndex = group.getSelected();

				if(themeIndex == 0)
					demo.setTheme(sy::steel);
				else if(themeIndex == 1)
					demo.setTheme(sy::waterMelon);
				else if(themeIndex == 2)
					demo.setTheme(sy::deepSea);
				else
					demo.setTheme(sy::royal);

			}

		}

		window.clear(sf::Color(240, 240, 240));

		//draw all gui elements
		sy::Window::drawWindows(window);

		window.display();

	}

	return 0;

}
