#include "Slider.h"
#include "Themes.h"
#include <math.h>

namespace sy
{

Slider::Slider(sf::Vector2f pos,
		sf::Vector2f size,
		float value) :
			value(value)
{
	theme = sy::steel;

	setPosition(pos);

	line.setSize(sf::Vector2f(size.x, 4));
	line.setOrigin(size.x/2.0f, 2);
	line.setFillColor(theme.primaryDark);

	float handleWidth = fmin(fmax(size.x*0.07, 8), 16);
	handle.setRadius(handleWidth);
	handle.setOrigin(handleWidth/2.0f, handleWidth);
	handle.setOutlineThickness(theme.thickness);
	handle.setOutlineColor(theme.outline);
	handle.setFillColor(theme.secondary);

	//this highlight does not have an outline
	//no matter what because it's drawn over negative space
	size.x += handleWidth;
	highlight.setSize(size);
	highlight.setOrigin(size/2.0f);
	highlight.setFillColor(sf::Color::Transparent);

	dragging = false;
	hovered = false;

}

Slider::Slider(const Slider& copy)
{
	setPosition(copy.getPosition());

	line = copy.line;

	handle = copy.handle;

	highlight = copy.highlight;

	dragging = copy.dragging;
	hovered = copy.hovered;
	value = copy.value;

}

void Slider::processEvent(sf::Event event)
{
	if(event.type == sf::Event::MouseMoved)
	{
		highlight.setFillColor(sf::Color::Transparent);
		sf::Vector2f mPos(event.mouseMove.x, event.mouseMove.y);
		sf::Vector2f pos = getOffset();
		if(dragging)
		{

			sf::FloatRect lineBounds = line.getGlobalBounds();
			lineBounds.left += pos.x;
			lineBounds.top += pos.y;

			if(mPos.x < lineBounds.left)
				mPos.x = lineBounds.left;
			else if(mPos.x > lineBounds.left + lineBounds.width)
				mPos.x = lineBounds.left + lineBounds.width;

			mPos.x -= lineBounds.left + lineBounds.width/2.0f;

			handle.setPosition(mPos.x, 0);

			value = (mPos.x/lineBounds.width) + 0.5;

		}

		sf::FloatRect bounds = highlight.getGlobalBounds();

		bounds.left += pos.x;
		bounds.top += pos.y;
		hovered = bounds.contains(mPos);
		if(hovered && !dragging)
		{
			highlight.setFillColor(theme.highlight);

		}

	}
	else if(event.type == sf::Event::MouseButtonPressed)
	{
		bool mouseLeft = event.mouseButton.button == sf::Mouse::Left;
		if(hovered && mouseLeft)
		{
			dragging = true;
			sf::Vector2f mPos(event.mouseButton.x, event.mouseButton.y);
			handle.setFillColor(theme.secondary);
			highlight.setFillColor(sf::Color::Transparent);

		}

	}
	else if(event.type == sf::Event::MouseButtonReleased)
	{
		bool mouseLeft = event.mouseButton.button == sf::Mouse::Left;
		if(mouseLeft)
		{
			dragging = false;
			handle.setFillColor(theme.secondary);

		}

	}

}

void Slider::draw(sf::RenderWindow& window,
		sf::RenderStates states)
{
	states.transform *= getTransform();
	window.draw(line, states);
	window.draw(highlight, states);
	window.draw(handle, states);

}

void Slider::setTheme(Theme theme)
{
	line.setOutlineColor(theme.primaryDark);

	handle.setFillColor(theme.secondary);

	handle.setOutlineThickness(theme.thickness);
	handle.setOutlineColor(theme.outline);

	Element::setTheme(theme);

}

}

