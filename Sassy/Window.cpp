#include "Window.h"
#include "Themes.h"

namespace sy
{

Window::ManagementMode Window::mode = Window::Internal;

std::vector<sy::Window*> Window::windowManager;
bool Window::inputCaptured = false;

Window::Window(sf::Vector2f pos,
		sf::Vector2f size,
		const sf::String &str,
		unsigned int charSize)
{
	theme = sy::steel;

	setPosition(pos);

	base.setSize(size);
	base.setOrigin(size/2.0f);
	base.setOutlineThickness(theme.thickness);
	base.setOutlineColor(theme.outline);
	base.setFillColor(theme.primary);

	bar.setSize(sf::Vector2f(size.x, 20));
	bar.setOrigin(size.x/2.0f, size.y/2.0f);
	bar.setOutlineThickness(theme.thickness);
	bar.setOutlineColor(theme.outline);
	bar.setFillColor(theme.focused);

	text = sf::Text(str, *theme.font, charSize);
	text.setColor(sf::Color::Black);
	text.setOrigin(size.x/2.0f, size.y/2.0f);

	highlight = base;
	highlight.setFillColor(sf::Color::Transparent);

	hovered = false;
	focused = false;
	hovered2 = false;
	dragging = false;
	visible = true;

	windowManager.push_back(this);

}

Window::Window(const Window& copy)
{
	setPosition(copy.getPosition());

	base = copy.base;

	bar = copy.bar;

	text = copy.text;

	highlight = copy.highlight;

	hovered = copy.hovered;
	focused = copy.focused;
	hovered2 = copy.hovered2;
	dragging = copy.dragging;
	visible = copy.visible;

	windowManager.push_back(this);

}

Window::~Window()
{
	std::vector<Window*>::iterator it;
	for(it = windowManager.begin(); it != windowManager.end(); it++)
	{
		if((*it) == this)
		{
			windowManager.erase(it);
			break;

		}

	}

}

void Window::processEvent(sf::Event event)
{
	if(visible)
	{
		if(event.type == sf::Event::MouseMoved)
		{
			highlight.setFillColor(sf::Color::Transparent);
			sf::Vector2f mPos(event.mouseMove.x, event.mouseMove.y);
			if(dragging)
			{
				setPosition(mPos+dragOffset);

			}

			sf::FloatRect bounds = base.getGlobalBounds();
			sf::Vector2f pos = getOffset();
			bounds.left += pos.x;
			bounds.top += pos.y;
			hovered = bounds.contains(mPos);
			if(hovered && !focused)
			{
				highlight.setFillColor(theme.highlight);

			}

			sf::FloatRect barBounds = bar.getGlobalBounds();
			barBounds.left += pos.x;
			barBounds.top += pos.y;
			hovered2 = barBounds.contains(mPos);

		}

		//internal and external management are the same in the processEvent method
		bool usingInOrEx = (mode == Internal || mode == External);

		if(mode == None || (usingInOrEx && !inputCaptured))
		{
			if(event.type == sf::Event::MouseButtonPressed)
			{
				bool mouseLeft = event.mouseButton.button == sf::Mouse::Left;
				focused = hovered && mouseLeft;
				bar.setFillColor(theme.focused);
				if(focused)
				{
					if(usingInOrEx)
					{
						setTop(this);
						inputCaptured = true;

					}

					bar.setFillColor(theme.primaryDark);
					highlight.setFillColor(sf::Color::Transparent);

					sf::Event mouseMove;
					mouseMove.type = sf::Event::MouseMoved;
					mouseMove.mouseMove.x = event.mouseButton.x;
					mouseMove.mouseMove.y = event.mouseButton.y;

					//fix for clicking on an element of a non active window
					processElementEvents(mouseMove);

				}

				dragging = hovered2 && mouseLeft;
				if(dragging)
				{
					sf::Vector2f mPos(event.mouseButton.x, event.mouseButton.y);
					dragOffset = getPosition() -  mPos;

				}

			}
			else if(event.type == sf::Event::MouseButtonReleased)
			{
				bool mouseLeft = event.mouseButton.button == sf::Mouse::Left;
				if(mouseLeft)
				{
					dragging = false;

				}

			}

			if(focused)
			{
				processElementEvents(event);

			}

		}

	}

}

void Window::draw(sf::RenderWindow& window,
		sf::RenderStates states)
{
	if(visible)
	{
		states.transform *= getTransform();

		window.draw(base, states);
		window.draw(bar, states);
		window.draw(text, states);

		drawElements(window, states);

		window.draw(highlight, states);

	}

	//hopefully you aren't magically intermixing processEvent and draw calls
	if(mode == Internal)
		inputCaptured = false;

}

void Window::setFocused(bool setFocused)
{
	focused = setFocused;
	if(setFocused)
	{
		setTop(this);
		bar.setFillColor(theme.primaryDark);

	}
	else
		bar.setFillColor(theme.focused);

}

void Window::setTheme(Theme theme)
{
	base.setFillColor(theme.primary);

	base.setOutlineThickness(theme.thickness);
	base.setOutlineColor(theme.outline);

	bar.setFillColor(theme.focused);

	bar.setOutlineThickness(theme.thickness);
	bar.setOutlineColor(theme.outline);

	if(theme.hasFont())
	{
		text.setFont(*theme.font);

	}

	highlight.setOutlineThickness(theme.thickness);
	highlight.setOutlineColor(theme.outline);

	Element::setTheme(theme);
	setUserTheme(theme);

}

void Window::setUserTheme(Theme theme)
{
	GUI::setUserTheme(theme);

}

void Window::processWindows(sf::Event event)
{
	inputCaptured = false;

	for(unsigned int i = 0; i < windowManager.size(); i++)
		windowManager[i]->processEvent(event);

}

void Window::drawWindows(sf::RenderWindow& window,
		sf::RenderStates states)
{
	//render the windows in reverse so the active window is drawn ontop
	for(int i = windowManager.size()-1; i >= 0; i--)
		windowManager[i]->draw(window, states);

}

void Window::setTop(Window* window)
{
	for(unsigned int i = 0; i < windowManager.size(); i++)
	{
		if(windowManager[i] != window)
			windowManager[i]->setFocused(false);

	}

	std::vector<Window*>::iterator it;
	for(it = windowManager.begin(); it != windowManager.end(); it++)
	{
		if((*it) == window)
		{
			windowManager.erase(it);
			break;

		}

	}

	windowManager.insert(windowManager.begin(), window);

}

}
