#include "RadioGroup.h"
#include "Themes.h"

namespace sy
{

RadioGroup::RadioGroup(sf::Vector2f pos,
		unsigned int num,
		bool vertical,
		unsigned int spacing) //spacing from the center in pixels
{
	theme = sy::steel;

	setPosition(pos);

	sf::Vector2f offset;
	for(unsigned int i = 0; i < num; i++)
	{
		bool ticked = i == 0;
		RadioButton button(offset, ticked);
		button.parent = this;
		buttons.push_back(button);
		if(vertical)
			offset.y += spacing;
		else
			offset.x += spacing;

	}


	switched = false;
	current = 0;

}

RadioGroup::RadioGroup(const RadioGroup& copy)
{
	setPosition(copy.getPosition());

	buttons = copy.buttons;
	switched = copy.switched;
	current = copy.current;

	//when you use the default copy constructor elements will keep their old parent pointer
	for(unsigned int i = 0; i < buttons.size(); i++)
	{
		buttons[i].parent = this;

	}

}

void RadioGroup::processEvent(sf::Event event)
{
	switched = false;
	for(unsigned int i = 0; i < buttons.size(); i++)
	{
		buttons[i].processEvent(event);
		if(current != i && buttons[i].ticked && !switched)
		{
			setSelected(i);
			switched = true;

		}

	}

}

void RadioGroup::draw(sf::RenderWindow& window,
		sf::RenderStates states)
{
	states.transform *= getTransform();
	for(unsigned int i = 0; i < buttons.size(); i++)
	{
		buttons[i].draw(window, states);

	}

}

bool RadioGroup::hasSwitched()
{
	return switched;

}

unsigned int RadioGroup::getSelected()
{
	return current;

}

void RadioGroup::setSelected(unsigned int index)
{
	if(current != index)
	{
		switched = true;
		buttons[current].ticked = false;
		current = index;
		buttons[index].ticked = true;

	}


}

RadioGroup::RadioButton::RadioButton(sf::Vector2f pos,
		bool ticked,
		float radius) :
			ticked(ticked)
{
	theme = sy::steel;

	setPosition(pos);

	base.setRadius(radius);
	base.setOrigin(radius, radius);
	base.setFillColor(theme.focused);
	base.setOutlineThickness(theme.thickness);
	base.setOutlineColor(theme.outline);

	tick.setRadius(radius/3.0);
	tick.setOrigin(radius/3.0, radius/3.0);
	tick.setFillColor(theme.secondary);

	highlight = base;
	highlight.setFillColor(sf::Color::Transparent);

	hovered = false;

}

//basically just the checkbox code copy pasta
void RadioGroup::RadioButton::processEvent(sf::Event event)
{
	if(!ticked)
	{
		if(event.type == sf::Event::MouseMoved)
		{
			highlight.setFillColor(sf::Color::Transparent);
			sf::Vector2f mPos(event.mouseMove.x, event.mouseMove.y);
			sf::FloatRect bounds = base.getGlobalBounds();
			sf::Vector2f pos = getOffset();
			bounds.left += pos.x;
			bounds.top += pos.y;
			hovered = bounds.contains(mPos);
			if(hovered)
			{
				highlight.setFillColor(theme.highlight);

			}

		}
		else if(hovered && event.type == sf::Event::MouseButtonPressed)
		{
			if(event.mouseButton.button == sf::Mouse::Left)
			{
				ticked = true;
				hovered = false;
				highlight.setFillColor(sf::Color::Transparent);

			}

		}

	}

}

void RadioGroup::RadioButton::draw(sf::RenderWindow& window,
		sf::RenderStates states)
{
	states.transform *= getTransform();
	window.draw(base, states);
	window.draw(base, states);
	if(ticked)
		window.draw(tick, states);

	window.draw(highlight, states);

}

void RadioGroup::RadioButton::setTheme(Theme theme)
{
	base.setFillColor(theme.focused);
	base.setOutlineThickness(theme.thickness);
	base.setOutlineColor(theme.outline);

	tick.setFillColor(theme.secondary);

	highlight.setOutlineThickness(theme.thickness);
	highlight.setOutlineColor(theme.outline);

	Element::setTheme(theme);

}

}
