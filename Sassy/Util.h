#ifndef UTIL_H_
#define UTIL_H_

#include <SFML/Graphics.hpp>

namespace sy
{

sf::Color hexToColor(unsigned int hex);

unsigned int colorToHex(sf::Color color);

//Default font is RobotoCondensedLight
sf::Font& getDefaultFont();

}

#endif /* UTIL_H_ */
