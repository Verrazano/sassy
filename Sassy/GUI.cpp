#include "GUI.h"
#include "Themes.h"

namespace sy
{

GUI::GUI()
{
	userTheme = steel;

}

GUI::~GUI()
{
	while(!elements.empty())
	{
		delete elements.begin()->second;
		elements.erase(elements.begin());

	}

}

Element& GUI::getElement(std::string name)
{
	return *elements[name];

}

void GUI::processElementEvents(sf::Event event)
{
	std::map<std::string, Element*>::iterator it;
	for(it = elements.begin(); it != elements.end(); it++)
	{
		it->second->processEvent(event);

	}

}

void GUI::drawElements(sf::RenderWindow& window,
		sf::RenderStates states)
{
	std::map<std::string, Element*>::iterator it;
	for(it = elements.begin(); it != elements.end(); it++)
	{
		it->second->draw(window, states);

	}

}

void GUI::setUserTheme(Theme theme)
{
	std::map<std::string, Element*>::iterator it;
	for(it = elements.begin(); it != elements.end(); it++)
	{
		it->second->setTheme(theme);

	}

	userTheme = theme;

}

void GUI::setFont(sf::Font* font)
{
	userTheme.font = font;
	setUserTheme(userTheme);

}

}

