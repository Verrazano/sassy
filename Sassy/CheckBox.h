#ifndef CHECKBOX_H_
#define CHECKBOX_H_

#include "Element.h"

namespace sy
{

class CheckBox : public Element
{
public:
	CheckBox(sf::Vector2f pos,
			bool checked = false,
			sf::Vector2f size = sf::Vector2f(32, 32));

	CheckBox(const CheckBox& copy);

	void processEvent(sf::Event event);

	void draw(sf::RenderWindow& window,
			sf::RenderStates states = sf::RenderStates::Default);

	void setTheme(Theme theme);

	sf::RectangleShape base;
	sf::RectangleShape check;
	sf::RectangleShape highlight;

	bool checked;
	bool hovered;

};

}

#endif /* CHECKBOX_H_ */
