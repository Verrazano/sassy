#ifndef THEME_H_
#define THEME_H_

#include <SFML/Graphics.hpp>

namespace sy
{

class Theme
{
public:
	Theme();

	Theme(sf::Font* font,
			unsigned int thickness = 1,
			sf::Color outline = sf::Color::Black,
			sf::Color primary = sf::Color::White,
			sf::Color primaryDark = sf::Color(230, 230, 230),
			sf::Color secondary = sf::Color::White,
			sf::Color focused = sf::Color::Cyan,
			sf::Color highlight = sf::Color(255, 255, 0, 100));

	Theme(sf::Font* font,
			unsigned int thickness,
			unsigned int outline,
			unsigned int primary,
			unsigned int primaryDark,
			unsigned int secondary,
			unsigned int focused,
			sf::Color highlight);

	bool hasFont();

	sf::Font* font;
	unsigned int thickness;
	sf::Color outline;
	sf::Color primary;
	sf::Color primaryDark;
	sf::Color secondary;
	sf::Color focused;
	sf::Color highlight;

};

}

#endif /* THEME_H_ */
