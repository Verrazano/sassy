#include "Label.h"
#include "Themes.h"

namespace sy
{

Label::Label(sf::Vector2f pos,
		const sf::String &str,
		unsigned int charSize)
{
	theme = sy::steel;

	setPosition(pos);

	text.setCharacterSize(charSize);
	text.setColor(sf::Color::Black);
	//sets the font based on the theme
	setString(str);

}

void Label::processEvent(sf::Event event)
{
}

void Label::draw(sf::RenderWindow& window,
		sf::RenderStates states)
{
	states.transform *= getTransform();
	window.draw(text, states);

}

void Label::setString(const sf::String &str)
{
	text.setString(str);
	sf::FloatRect bounds = text.getGlobalBounds();
	text.setOrigin(bounds.width/2.0f, bounds.height/2.0f);

}

void Label::setTheme(Theme theme)
{
	if(theme.hasFont())
	{
		text.setFont(*theme.font);
		setString(text.getString());

	}

	Element::setTheme(theme);

}

}
