template <class T>
T& Window::addElement(std::string name,
		T element)
{
	T& real = GUI::addElement(name, element);
	real.move(-base.getSize()/2.0f);
	real.parent = this;
	return real;

}
