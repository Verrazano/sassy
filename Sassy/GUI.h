#ifndef GUI_H_
#define GUI_H_

#include <map>
#include "Element.h"

namespace sy
{

class GUI
{
public:
	GUI();

	virtual ~GUI();

	template <class T>
	T& addElement(std::string name,
			T element);

	Element& getElement(std::string name);

	virtual void processElementEvents(sf::Event event);

	void drawElements(sf::RenderWindow& window,
			sf::RenderStates states = sf::RenderStates::Default);

	void setUserTheme(Theme theme);

	void setFont(sf::Font* font);

	std::map<std::string, Element*> elements;
	Theme userTheme;

};

#include "GUI.inl"

}

#endif /* GUI_H_ */
