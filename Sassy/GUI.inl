template <class T>
T& GUI::addElement(std::string name,
		T element)
{
	//this could cause problems if somebody is holding a reference to an object they over write
	if(elements[name] != NULL)
		delete elements[name];

	T* real = new T(element);
	real->setTheme(userTheme);

	elements[name] = real;

	return *real;

}
