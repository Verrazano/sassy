#include "CheckBox.h"
#include "GUI.h"
#include "Themes.h"

namespace sy
{

CheckBox::CheckBox(sf::Vector2f pos,
		bool checked,
		sf::Vector2f size) :
			checked(checked)
{
	theme = sy::steel;

	setPosition(pos);

	base.setSize(size);
	base.setOrigin(size/2.0f);
	base.setOutlineThickness(theme.thickness);
	base.setOutlineColor(theme.outline);
	base.setFillColor(theme.focused);

	check.setSize(size*0.7f);
	check.setOrigin(size*0.7f/2.0f);
	check.setFillColor(theme.secondary);

	highlight = base;
	highlight.setFillColor(sf::Color::Transparent);

	hovered = false;

}

CheckBox::CheckBox(const CheckBox& copy)
{
	setPosition(copy.getPosition());

	base = copy.base;

	check = copy.check;

	highlight = copy.highlight;

	hovered = copy.hovered;
	checked = copy.checked;

}

void CheckBox::processEvent(sf::Event event)
{
	if(event.type == sf::Event::MouseMoved)
	{
		highlight.setFillColor(sf::Color::Transparent);
		sf::Vector2f mPos(event.mouseMove.x, event.mouseMove.y);
		sf::FloatRect bounds = base.getGlobalBounds();
		sf::Vector2f pos = getOffset();
		bounds.left += pos.x;
		bounds.top += pos.y;
		hovered = bounds.contains(mPos);
		if(hovered)
		{
			highlight.setFillColor(theme.highlight);

		}

	}
	else if(hovered && event.type == sf::Event::MouseButtonPressed)
	{
		if(event.mouseButton.button == sf::Mouse::Left)
		{
			checked = !checked;
			highlight.setFillColor(sf::Color::Transparent);

		}

	}

}

void CheckBox::draw(sf::RenderWindow& window,
		sf::RenderStates states)
{
	states.transform *= getTransform();
	window.draw(base, states);
	if(checked)
		window.draw(check, states);
	window.draw(highlight, states);

}

void CheckBox::setTheme(Theme theme)
{
	base.setFillColor(theme.focused);
	base.setOutlineThickness(theme.thickness);
	base.setOutlineColor(theme.outline);

	check.setFillColor(theme.secondary);

	highlight.setOutlineThickness(theme.thickness);
	highlight.setOutlineColor(theme.outline);

	Element::setTheme(theme);

}

}

