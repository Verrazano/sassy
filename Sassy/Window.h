#ifndef WINDOW_H_
#define WINDOW_H_

#include "GUI.h"
#include <iostream>

namespace sy
{

class Window : public GUI, public Element
{
public:
	Window(sf::Vector2f pos,
			sf::Vector2f size,
			const sf::String &str,
			unsigned int charSize = 20);

	Window(const Window& copy);

	~Window();

	template <class T>
	T& addElement(std::string name,
			T element);

	void processEvent(sf::Event event);

	void draw(sf::RenderWindow& window,
			sf::RenderStates states = sf::RenderStates::Default);

	//using this function has the potential to cause multiple windows to be focused
	//since it doesn't tirgger a mouse clicked event
	void setFocused(bool setFocused = true);

	void setTheme(Theme theme);

	void setUserTheme(Theme theme);

	bool visible;

	bool hovered;
	bool focused;

	bool hovered2;
	bool dragging;
	sf::Vector2f dragOffset;

	sf::RectangleShape base;
	sf::RectangleShape bar;
	sf::Text text;
	sf::RectangleShape highlight;

	typedef enum ManagementMode
	{
		None = 0, //Bad things can happen
		Internal, //Tries it's best to use inputs correctly (can't draw windows in correct order), use internal if you intend to handle windows separately.
		External //Uses static process/draw methods to handle inputs/order windows

	} ManagementMode;

	static ManagementMode mode;

	static void processWindows(sf::Event event);
	static void drawWindows(sf::RenderWindow& window,
			sf::RenderStates states = sf::RenderStates::Default);

protected:
	static std::vector<Window*> windowManager;
	static bool inputCaptured;

	static void setTop(Window* window);

};

#include "Window.inl"

}

#endif /* WINDOW_H_ */
