#ifndef RADIOGROUP_H_
#define RADIOGROUP_H_

#include "Element.h"

namespace sy
{

class RadioGroup : public Element
{
public:
	RadioGroup(sf::Vector2f pos,
			unsigned int num,
			bool vertical = true,
			unsigned int spacing = 32); //spacing from the center in pixels

	RadioGroup(const RadioGroup& copy);

	void processEvent(sf::Event event);

	void draw(sf::RenderWindow& window,
			sf::RenderStates states = sf::RenderStates::Default);

	bool hasSwitched();

	unsigned int getSelected();

	void setSelected(unsigned int index);

	bool switched;
	unsigned int current;

protected:
	/*This class is protected because a RadioButton is
	 * useless without a RadioGroup.*/
	class RadioButton : public Element
	{
	public:
		RadioButton(sf::Vector2f pos,
				bool ticked = false,
				float radius = 12);

		void processEvent(sf::Event event);

		void draw(sf::RenderWindow& window,
				sf::RenderStates states = sf::RenderStates::Default);

		void setTheme(Theme theme);

		sf::CircleShape base;
		sf::CircleShape tick;
		sf::CircleShape highlight;

		bool ticked;
		bool hovered;

	};

	std::vector<RadioButton> buttons;

};

}

#endif /* RADIOGROUP_H_ */
