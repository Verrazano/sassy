#ifndef THEMES_H_
#define THEMES_H_

#include "Theme.h"
#include "Util.h"

namespace sy
{

static Theme royal(&getDefaultFont(), 1, 0x0, 0x673ab7, 0x512da8, 0xffc107, 0xd1c4e9, sf::Color(0, 0, 0, 96));
static Theme royalBorderless(&getDefaultFont(), 0, 0x0, 0x673ab7, 0x512da8, 0xffc107, 0xd1c4e9, sf::Color(0, 0, 0, 96));
static Theme royalLight(&getDefaultFont(), 1, 0x0, 0xffffff, 0x673ab7, 0xffc107, 0xd1c4e9, sf::Color(0, 0, 0, 96));

static Theme tigerEye(&getDefaultFont(), 1, 0x0, 0xffc107, 0xffa000, 0x795548, 0xffecb3, sf::Color(0, 0, 0, 96));
static Theme tigerEyeBorderless(&getDefaultFont(), 0, 0x0, 0xffc107, 0xffa000, 0x795548, 0xffecb3, sf::Color(0, 0, 0, 96));
static Theme tigerEyeLight(&getDefaultFont(), 1, 0x0, 0xffffff, 0xffc107, 0x795548, 0xffecb3, sf::Color(0, 0, 0, 96));

static Theme waterMelon(&getDefaultFont(), 1, 0x0, 0x009688, 0x00796b, 0xff4081, 0xb2dfdb, sf::Color(0, 0, 0, 96));
static Theme waterMelonBorderless(&getDefaultFont(), 0, 0x0, 0x009688, 0x00796b, 0xff4081, 0xb2dfdb, sf::Color(0, 0, 0, 96));
static Theme waterMelonLight(&getDefaultFont(), 1, 0x0, 0xffffff, 0x009688, 0xff4081, 0xb2dfdb, sf::Color(0, 0, 0, 96));

static Theme deepSea(&getDefaultFont(), 1, 0x0, 0x3f51b5, 0x303f9f, 0x8bc34a, 0xc5cae9, sf::Color(0, 0, 0, 96));
static Theme deepSeaBorderless(&getDefaultFont(), 0, 0x0, 0x3f51b5, 0x303f9f, 0x8bc34a, 0xc5cae9, sf::Color(0, 0, 0, 96));
static Theme deepSeaLight(&getDefaultFont(), 1, 0x0, 0xffffff, 0x3f51b5, 0x8bc34a, 0xc5cae9, sf::Color(0, 0, 0, 96));

static Theme steel(&getDefaultFont(), 1, 0x0, 0x607d8b, 0x55a64, 0x03a9f4, 0xcfd8dc, sf::Color(0, 0, 0, 30));

}

#endif /* THEMES_H_ */
